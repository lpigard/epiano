#include <time.h>
#include <stdint.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_timer.h>

const int sample_freq = 44100;
const int n_keys = 10;
long int t_last_pressed[n_keys];
float f0[n_keys];
int key_is_pressed[n_keys];

void audio_callback(void* userdata, uint8_t* stream, int len)
{
    uint64_t* samples_played = (uint64_t*)userdata;
    float* fstream = (float*)(stream);

    // printf("%g\n", (*samples_played - t_last_pressed) / 44100.);

    for(int sid = 0; sid < (len / 8); sid++)
    {
        float amp = 0;

        for(int k = 0; k < n_keys; k++)
        {
            float time = (*samples_played + sid - t_last_pressed[k]) / (float)sample_freq;
            // printf("%g\n", time);
            const float v0 = 0.2;
            // float volume[10] = {1, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            float volume[10] = {1., 0.1, 0.325, 0.05, 0.05, 0.05, 0, 0.02, 0, 0};
            // float volume[10] = {1., 1., 1., 1., 1., 1., 1., 1., 1., 1.};

            for(int i = 0; i < 10; i++)
            {
                amp += v0 * volume[i] * sin(f0[k] * (i + 1) * 2.0 * M_PI * time) * exp(-2. * time * f0[k] * (i + 1) / f0[0]);
            }

            // fstream[sid] = amp; /* L */
        }

        fstream[2 * sid + 0] = amp; /* L */
        fstream[2 * sid + 1] = amp; /* R */

    }


    *samples_played += (len / 8);
}

int main(int argc, char* argv[])
{
    uint64_t samples_played = 0;

    if(SDL_Init(SDL_INIT_AUDIO) < 0)
    {
        fprintf(stderr, "Error initializing SDL. SDL_Error: %s\n", SDL_GetError());
        return -1;
    }


    SDL_AudioSpec audio_spec_want, audio_spec;
    SDL_memset(&audio_spec_want, 0, sizeof(audio_spec_want));

    audio_spec_want.freq     = sample_freq;
    audio_spec_want.format   = AUDIO_F32;
    audio_spec_want.channels = 1;
    audio_spec_want.samples  = 512;
    audio_spec_want.callback = audio_callback;
    audio_spec_want.userdata = (void*)&samples_played;

    SDL_AudioDeviceID audio_device_id = SDL_OpenAudioDevice(
        NULL, 0,
        &audio_spec_want, &audio_spec,
        SDL_AUDIO_ALLOW_FORMAT_CHANGE
    );

    if(!audio_device_id)
    {
        fprintf(stderr, "Error creating SDL audio device. SDL_Error: %s\n", SDL_GetError());
        SDL_Quit();
        return -1;
    }

    int window_width  = 600;
    int window_height = 600;
    SDL_Window* window;
    {
        window = SDL_CreateWindow(
            "SDL Tone Generator",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            window_width, window_height,
            SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE
        );

        if(!window)
        {
            fprintf(stderr, "Error creating SDL window. SDL_Error: %s\n", SDL_GetError());
            SDL_Quit();
            return -1;
        }
    }

    SDL_PauseAudioDevice(audio_device_id, 0);

    for(int k = 0; k < n_keys; k++)
    {
        f0[k] = 261.625565301 * 2 * pow(2., k / 12.);
        t_last_pressed[k] = -1000000;
        key_is_pressed[k] = 0;

        printf("%d %g %ld %d\n", k, f0[k], t_last_pressed[k], key_is_pressed[k]);
    }

    SDL_Rect piano_key;
	piano_key.x = 100;
	piano_key.y = 400;
	piano_key.w = 20;
	piano_key.h = 100;


    int running = 1;
    while(running)
    {
        // Process input
        SDL_Event event;

        //single-hit keys, mouse, and other general SDL events (eg. windowing)
        while(SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_QUIT:
                running = 0;
                break;
                case SDL_KEYDOWN:
                for(int k = 0; k < n_keys; k++)
                {
                    if(event.key.keysym.sym == '0' + k && !key_is_pressed[k])
                    {
                        key_is_pressed[k] = 1;
                        printf("%d IS PRESSED!\n", k);
                        t_last_pressed[k] = *((uint64_t*)audio_spec_want.userdata);

                    }
                }
                break;

                case SDL_KEYUP:
                for(int k = 0; k < n_keys; k++)
                {
                    if(event.key.keysym.sym == '0' + k && key_is_pressed[k])
                    {
                        key_is_pressed[k] = 0;
                    }
                }
                break;
            }
        }

        // printf("TEST\n");
    }

    SDL_DestroyWindow(window);
    SDL_CloseAudioDevice(audio_device_id);
    SDL_Quit();

    return 0;
}
